package org.example.repository;

import org.example.entity.User;

import java.util.List;

public interface UserRepository {
    public User findByUsername(String username);

    public User findByUserId(Long userId);

    public void deleteByUserId(Long userId);

    public List<User> getUserList();
}
