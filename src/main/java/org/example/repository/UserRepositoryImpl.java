package org.example.repository;

import org.example.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UserRepositoryImpl implements UserRepository{
    private static List<User> userList = populateList();

    static List<User> populateList(){
        List<User>userList = new ArrayList<>();
        Random random = new Random();
        for (int i = 1; i <= 10; i++) {
            userList.add(new User((long) i, "user" + i, random.nextBoolean()));
        }
        return userList;
    }

    public User findByUsername(String username){
        for(User u : userList){
            if(u.getUsername().equals(username)){
                return u;
            }
        }
        return null;
    }

    public User findByUserId(Long userId){
        for(User u : userList){
            if(u.getUserId().equals(userId)){
                return u;
            }
        }
        return null;
    }

    public void deleteByUserId(Long userId){
        User userToDelete = null;
        for(User u : userList){
            if(u.getUserId().equals(userId)){
                userToDelete = u;
            }
        }
        userList.remove(userToDelete);
    }

    @Override
    public List<User> getUserList() {
        return userList;
    }
}
