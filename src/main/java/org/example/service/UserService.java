package org.example.service;

import org.example.entity.User;

import java.util.List;

public interface UserService {
    public boolean isUserActive(String username);

    public void deleteUser(Long userId);

    public User getUser(Long userId);

    public List<User> getAllUsers();
}
