package org.example.service;

import org.example.entity.User;
import org.example.repository.UserRepository;

import java.util.List;

public class UserServiceImpl implements UserService{
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean isUserActive(String username) {
        User user = userRepository.findByUsername(username);
        if(user == null){
            throw new RuntimeException("User not found: so no active state");
        }
        return user.isActive();
    }

    @Override
    public void deleteUser(Long userId) {
        User user = userRepository.findByUserId(userId);
        if (user == null) {
            throw new RuntimeException("User Not found: user cannot be deleted");
        }
        userRepository.deleteByUserId(userId);
    }

    @Override
    public User getUser(Long userId) {
        User user = userRepository.findByUserId(userId);
        if(user == null)
            throw new RuntimeException("User not found: user cannot be retrieved");
        return user;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.getUserList();
    }
}

