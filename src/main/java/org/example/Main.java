package org.example;

import org.example.entity.User;
import org.example.repository.UserRepositoryImpl;
import org.example.service.UserService;
import org.example.service.UserServiceImpl;

public class Main {
    public static void main(String[] args) {
        UserService userService = new UserServiceImpl(new UserRepositoryImpl());

        for(User u : userService.getAllUsers()){
            System.out.println(u);
        }
        System.out.println();
        try {
            System.out.println("User with id of 1 was deleted");
            userService.deleteUser(1L);

            System.out.println("User with id of 5 was deleted");
            userService.deleteUser(5L);

            System.out.println("Active status of user with user3: " + userService.isUserActive("user3"));

            System.out.println("User with id of 2: " + userService.getUser(2L));
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        System.out.println();
        for(User u : userService.getAllUsers()){
            System.out.println(u);
        }
    }
}