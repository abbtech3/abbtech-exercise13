package org.example;

import org.example.entity.User;
import org.example.repository.UserRepository;
import org.example.service.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class UserServiceUnitTest {

    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private UserServiceImpl userService;


    @ParameterizedTest
    @CsvSource(value = {"user", "user1", "user2"})
    void testIsUserActive_UserFoundAndActive(String username){
        User user = new User(1L, username, true);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);

        boolean actual = userService.isUserActive(username);

        Assertions.assertTrue(actual);
        Mockito.verify(userRepository, Mockito.times(1)).findByUsername(username);
    }

    @ParameterizedTest
    @CsvSource(value = {"user", "user1", "user2"})
    void testIsUserActive_UserFoundAndNotActive(String username){
        User user = new User(1L, username, false);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);

        boolean actual = userService.isUserActive(username);

        Assertions.assertFalse(actual);
        Mockito.verify(userRepository, Mockito.times(1)).findByUsername(username);
    }

    @ParameterizedTest
    @CsvSource(value = {"user", "user1", "user2"})
    void testIsUserActive_UserNotFound(String username){
        Mockito.when(userRepository.findByUsername(username)).thenReturn(null);

        Exception exception = Assertions.assertThrows(RuntimeException.class,
                () -> userService.isUserActive(username));

        String expectedMessage = "User not found: so no active state";
        String actualMessage = exception.getMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);
        Mockito.verify(userRepository, Mockito.times(1)).findByUsername(username);
    }

    @Test
    void testIsUserActive_UserNotFoundUsernameIsNull(){
        Mockito.when(userRepository.findByUsername(null)).thenReturn(null);

        Exception exception = Assertions.assertThrows(RuntimeException.class,
                () -> userService.isUserActive(null));

        String expectedMessage = "User not found: so no active state";
        String actualMessage = exception.getMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);
        Mockito.verify(userRepository, Mockito.times(1)).findByUsername(null);
    }

    @Test
    void testDeleteUser_UserFoundSuccessfulDeletion(){
        User user = new User(1L, "user", true);
        Mockito.when(userRepository.findByUserId(1L)).thenReturn(user);

        Assertions.assertDoesNotThrow(() -> userService.deleteUser(1L));
        Mockito.verify(userRepository, Mockito.times(1)).findByUserId(1L);
        Mockito.verify(userRepository, Mockito.times(1)).deleteByUserId(1L);
    }

    @Test
    void testDeleteUser_UserNotFoundFailedDeletion(){
        Mockito.when(userRepository.findByUserId(2L)).thenReturn(null);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> {
            userService.deleteUser(2L);
        });

        String expectedMessage = "User Not found: user cannot be deleted";
        String actualMessage = exception.getMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);

        Mockito.verify(userRepository, Mockito.times(1)).findByUserId(2L);
        Mockito.verify(userRepository, Mockito.never()).deleteByUserId(2L);
    }

    @Test
    void testDeleteUser_UserNotFoundIdIsNullFailedDeletion(){
        Mockito.when(userRepository.findByUserId(null)).thenReturn(null);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> {
            userService.deleteUser(null);
        });

        String expectedMessage = "User Not found: user cannot be deleted";
        String actualMessage = exception.getMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);

        Mockito.verify(userRepository, Mockito.times(1)).findByUserId(null);
        Mockito.verify(userRepository, Mockito.never()).deleteByUserId(null);
    }

    @Test
    public void testGetUser_UserFound() {
        User user = new User(1L, "user", true);
        Mockito.when(userRepository.findByUserId(1L)).thenReturn(user);

        User actual = userService.getUser(1L);

        Assertions.assertEquals(user, actual);

        Mockito.verify(userRepository, Mockito.times(1)).findByUserId(1L);
    }


    @Test
    public void testGetUser_UserNotFound() {
        Mockito.when(userRepository.findByUserId(2L)).thenReturn(null);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> {
            userService.getUser(2L);
        });

        String expectedMessage = "User not found: user cannot be retrieved";
        String actualMessage = exception.getMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);

        Mockito.verify(userRepository, Mockito.times(1)).findByUserId(2L);
    }
    @Test
    public void testGetUser_UserIdIsNullUserNotFound() {
        Mockito.when(userRepository.findByUserId(null)).thenReturn(null);

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> {
            userService.getUser(null);
        });

        String expectedMessage = "User not found: user cannot be retrieved";
        String actualMessage = exception.getMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);

        Mockito.verify(userRepository, Mockito.times(1)).findByUserId(null);
    }
    @Test
    public void testGetAllUsers_ListIsFull(){
        List<User> expectedUsers = new ArrayList<>();
        expectedUsers.add(new User(1L, "user1", false));
        expectedUsers.add(new User(2L, "user2", true));
        expectedUsers.add(new User(3L, "user3", true));


        Mockito.when(userRepository.getUserList()).thenReturn(expectedUsers);

        List<User> actualUsers = userService.getAllUsers();

        Assertions.assertEquals(expectedUsers, actualUsers);
        Mockito.verify(userRepository, Mockito.times(1)).getUserList();
    }

    @Test
    public void testGetAllUsers_ListIsEmpty(){
        List<User> expectedUsers = new ArrayList<>();

        Mockito.when(userRepository.getUserList()).thenReturn(expectedUsers);

        List<User> actualUsers = userService.getAllUsers();

        Assertions.assertEquals(expectedUsers, actualUsers);
        Mockito.verify(userRepository, Mockito.times(1)).getUserList();
    }

    @Test
    public void testGetAllUsers_ListIsNull(){
        List<User> expectedUsers = null;

        Mockito.when(userRepository.getUserList()).thenReturn(expectedUsers);

        List<User> actualUsers = userService.getAllUsers();

        Assertions.assertEquals(expectedUsers, actualUsers);
        Mockito.verify(userRepository, Mockito.times(1)).getUserList();
    }
}
